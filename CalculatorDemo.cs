﻿using System;
using System.Collections.Generic;

class CalculatorDemo
{

    static List<string> operations = new List<string> { "+", "-", "*", "/", "%",
                                                       "pow", "sqrt", "log", "!", "sin", "cos", "tan"};

    static void Main()
    {
        Console.WriteLine("Welcome to the calculator!");
        do
        {
            Console.Write("What do you want to do? Enter an operation (or 0 to quit): ");
            string operation = Console.ReadLine();
            if (operation == "0")
            {
                Console.WriteLine("Goodbye!");
                break;
            }

            if (!operations.Contains(operation))
                Console.WriteLine("Incorrect operation! Try again.");


            Calculator calculator = new Calculator();
            int x, y;
            switch (operation)
            {
                case "+":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    Console.WriteLine($"result = {calculator.Add(x, y)}");
                    break;
                case "-":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    Console.WriteLine($"result = {calculator.Substract(x, y)}");
                    break;
                case "*":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    Console.WriteLine($"result = {calculator.Multiply(x, y)}");
                    break;
                case "/":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    if (y == 0)
                    {
                        Console.WriteLine("Division by zero");
                    }
	                else {
                        Console.WriteLine($"result = {calculator.Divide(x, y)}");
                    }
                    break;
                case "%":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    if (y == 0)
                    {
                        Console.WriteLine("Division by zero");
                    }
                    else
                    {
                        Console.WriteLine($"result = {calculator.Remainder(x, y)}");
                    }
                    break;
                case "pow":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    Console.WriteLine($"result = {calculator.ToPow(x, y)}");
                    break;
                case "sqrt":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    if (x < 0 && y%2 == 0 || y == 0 || x == 0 && y < 0)
                        Console.WriteLine("Mathematically impossible");
                    else
                        Console.WriteLine($"result = {calculator.TakeRoot(x, y)}");
                    break;
                case "log":
                    x = GetNumber("x=");
                    y = GetNumber("y=");
                    if (x < 0 || y < 0 || y == 1) {
                        Console.WriteLine("Mathematically impossible");
                    } else {
                        Console.WriteLine($"result = {calculator.Log(x, y)}");
                    }
                    break;
                case "!":
                    x = GetNumber("x=");
                    if (x < 0)
                        Console.WriteLine("Factorials exists for non-negative numbers only");
                    else
                        Console.WriteLine($"result = {calculator.Factorial(x)}");
                    break;
                case "sin":
                    x = GetNumber("x=");
                    Console.WriteLine($"result = {calculator.Sin(x)}");
                    break;
                case "cos":
                    x = GetNumber("x=");
                    Console.WriteLine($"result = {calculator.Cos(x)}");
                    break;
                case "tan":
                    x = GetNumber("x=");
                    if (x == Math.PI / 2)
                        Console.WriteLine("Doesn't exist");
                    else
                        Console.WriteLine($"result = {calculator.Tan(x)}");
                    break;

            }
        } while (true);
    }

    public static int GetNumber(string msg)
    {
        int num;
        do
        {
            Console.Write(msg);
        } while (!int.TryParse(Console.ReadLine(), out num));
        return num;
    }
}

