﻿using System;


class Calculator
{
	public Calculator()
	{
	}

    public int Add(int x, int y)
    {
        return x + y;
    }

    public int Substract(int x, int y)
    {
        return x - y;
    }
    
    public int Multiply(int x, int y)
    {
        return x * y;
    }

    public int Divide(int x, int y)
    {
        return x / y;
    }

    public double Sin(int x)
    {
        return Math.Sin(x);
    }

    public int Factorial(int x)
    {
        if (x == 0) return 1;

        int res = x;
        while (x > 1)
        {
            --x;
            res *= x;
        }
        return res;
    }

    public int Remainder(int x, int y)
    {
        return x % y;
    }

    public double ToPow(int x, int y)
    {
        return Math.Pow(x, y);
    }
    
    public double TakeRoot(int x, int y)
    {
        int k = 1;
        if(x < 0)
        {
            x = -x;
            k = -1;
        }
        return Math.Pow(x, 1.0 / y) * k;
    }
    
    public double Cos(int x)
    {
        return Math.Cos(x);
    }
    
    public double Tan(int x)
    {
        return Math.Tan(x);
    }
    
    public double Log(int x, int y)
    {
        return Math.Log(x, y);
    }
}

